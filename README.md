1.  工程创建
     打开Vivado软件，创建名为project_LED的工程文件，由于PL端的设计属于寄存器传输级的工程，所以工程类型选择RTL Project,Target language选择Verilog，Simulator language选择Mixed，选择芯片型号时由于我们使用的是AX7020型号的ZYNQ，所以Family选择Zynq-7000，Speed grade选择-2,Package选择clg400,完成工程的创建。

2.  创建led.v文件并进行编辑
3.  创建名为led_test.v的仿真文件并进行编辑
4.  点击Run Simulation进行仿真
5.  点击Open Elaborated Design查看原理图，然后进行引脚设置
6.  点击Run Synthesis进行综合，然后创建时钟约束（为了得到功耗的估算），将sys_clk设置为50MHz,再重新进行综合
7.  点击Run Implementation进行实现
8.  创建比特流文件，将开发板通上电源并连接到主机，打开硬件管理器，写入程序，然后就可以在开发板上看到LED灯的闪烁
