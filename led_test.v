`timescale 1ns/1ns

module led_test(

    );
    
    reg sys_clk;
    wire [3:0]led;
    
    initial
    begin
        sys_clk = 0;
    end
    
    always #10 sys_clk <= ~sys_clk;
    
    led led_test(
        .sys_clk(sys_clk),
        .led(led)
    );
    
    
endmodule